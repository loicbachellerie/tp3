Jeffrey Guérin
Loic Bachellerie

Options pour l’entraînement :
•	-e : entraînement
•	-t <taille> : taille de fenêtre. <taille> doit suivre -t, précédé d’un espace.
•	-enc <encodage> : encodage de fichier. <encodage> doit suivre -enc, précédé d’un espace.
•	-cc <chemin> : chemin du corpus d’entraînement. <chemin> doit suivre -cc, précédé d’un espace.

Exemple:
-e -t 5 -enc UTF-8 -cc "textes\GerminalUTF8.txt"

Options pour la recherche :
•	-s : rechercher des synonymes
•	-t <taille> : taille de fenêtre. <taille> doit suivre -t, précédé d’un espace.

Exemple:
-s -t 5

