DROP TABLE lien;
DROP TABLE dictionnaire;


CREATE TABLE dictionnaire(
  ID int PRIMARY KEY ,
  mot varchar2(255)
);


CREATE TABLE lien (
 idLien int PRIMARY KEY,
 
 motID1 int NOT NULL REFERENCES dictionnaire(ID),
 motID2 int NOT NULL REFERENCES dictionnaire(ID),
 
 taille int NOT NULL,
 valeur int NOT NULL
);

SELECT * from d.dictionnaire INNER JOIN l.lien ON d.ID = l.motID1;


SELECT dic1.mot, dic2.mot, li.taille, li.valeur FROM lien li 
JOIN dictionnaire dic1 ON li.idLien=dic1.id
JOIN dictionnaire dic2 ON li.idLien=dic2.id;



UPDATE lien SET valeur = :v WHERE motID1 = :id1 AND motID2 = :id2 AND taille = :t ;

SELECT id FROM dictionnaire WHERE mot = 'camion';

SELECT motID1, motID2, valeur FROM lien WHERE taille = 5;





