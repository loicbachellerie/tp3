import sys, numpy as np
import re
import cx_Oracle as cx
from operator import itemgetter

def connectionOracle():
    PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'
    sys.path.append(PATH_ORACLE)
    import cx_Oracle
    dsn_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
    chaineConnexion = 'e6129194' + '/' + "freebachelle" + '@' + dsn_tns
    connexion = cx_Oracle.connect(chaineConnexion)
       
    return connexion
    
def lire(path, enc):
    f = open(path, 'r', encoding=enc)
    s = f.read()
    if s[0] == "lufeff":
        s = s[1:]
    f.close()
    return s

def filtre(texte):
    #print("\n Texte du fichier : " + texte)
    c = re.compile('[-;:,.?!/1234567^@89_0()*" ""\n\xa0"\']+', re.IGNORECASE)
    corpus = c.split(texte.lower())
    #print ("\n Texte du corpus : " + str(corpus[:]))
    return corpus

def filtre2(texte):
    d = {}
    c = re.compile('[,\n\r ]+', re.IGNORECASE)
    for mot in c.split(texte):
        d[mot] = 1
    return d

def dictionnaireBD(connexion):
    vocabulaire = {}
    cur = connexion.cursor()
    cur.execute('SELECT * FROM dictionnaire')
    resultatCur = cur.fetchall()
    index = 1
    for mot in resultatCur:
        vocabulaire[mot[1]] = mot[0] 
        index += 1
    cur.close()
    return vocabulaire
        
def dictionnaire(corpus, listDesMotsARajouterDansBD, vocabulaire):  # INSERE LES MOTS DANS LE DICTIONNAIRE
    index = len(vocabulaire)
    for mot in corpus:
        if mot not in vocabulaire and mot != '':
            vocabulaire[mot] = index
            listDesMotsARajouterDansBD.append((index, mot))
            index += 1
    return vocabulaire

def pourBDEcrireLesNouveauxMots(connexion, listDesMotsARajouterDansBD):
    cur = connexion.cursor()
    query = "INSERT INTO dictionnaire VALUES (:1, :2)"
    cur.executemany(query, listDesMotsARajouterDansBD, batcherrors=True)
    cur.close()
    connexion.commit()

def stopListe():
    stopliste = lire("stopliste.txt", "utf-8")
    stopliste = filtre2(stopliste)
    return stopliste

def creerDictionnaireCooccurrence(connexion):
    dictionnaireCooccurrence = {}
    cur = connexion.cursor()
    cur.execute('SELECT motID1, motID2, taille, valeur  FROM lien')
    donneeDeLaTableLien = cur.fetchall()
    for i in donneeDeLaTableLien:
        dictionnaireCooccurrence[(i[0], i[1], i[2])] = i[3]
    cur.close()
    return dictionnaireCooccurrence

def creerLes2Dictionnaires(connexion, tailleF, corpus, vocabulaire, dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate):
    debut = 0
    fin = tailleF
    milieu = fin // 2  # Milieu Fenetre
    
    while fin < len(corpus):  # DEPLACE LA FENETRE DANS LE CORPUS
        indexMot = vocabulaire[corpus[milieu]]
        i = debut  # REMET LE i AU DÉBUT DE LA FENETRE
        while i < fin :  # SE DEPLACE DANS LA FENETRE
            if i != milieu:
                indexCooc = vocabulaire[corpus[i]]
                #matrice[indexMot][indexCooc] += 1

                if (indexMot, indexCooc ,tailleF) not in dictionnairePourRequeteUpdate:
                    if (indexMot, indexCooc ,tailleF) not in dictionnairePourRequeteInsert:
                        dictionnairePourRequeteInsert[(indexMot, indexCooc ,tailleF)] = 1
                    else:
                        dictionnairePourRequeteInsert[(indexMot, indexCooc ,tailleF)] += 1
                elif (indexMot, indexCooc ,tailleF) in dictionnairePourRequeteUpdate:
                    dictionnairePourRequeteUpdate[(indexMot, indexCooc ,tailleF)] += 1
            i += 1         
        debut += 1    
        milieu += 1
        fin += 1
    return (dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate)

def creerMatrice(tailleF, dictionnairePourMatrice, vocabulaire, matrice):
    for cooccurrence, valeur in dictionnairePourMatrice.items():
        if cooccurrence[2] == tailleF:
            matrice[cooccurrence[0]][cooccurrence[1]] = valeur
    return matrice

############# METHODES DE CALCULS #############
def scalaire(vMot, vCooc):
    return (np.vdot(vMot, vCooc))
def cityblocks(vMot, vCooc):
    return np.sum(np.absolute(vMot - vCooc))
def leastsquares(vMot, vCooc):
    return np.sum((vMot-vCooc)**2)
def selectFunction(useFunction):
    if useFunction == "0":
        fonc = scalaire
        return fonc
    elif useFunction == "1":
        fonc = cityblocks
        return fonc
    elif useFunction == "2":
        fonc = leastsquares
        return fonc

########################## LISTER LES RESULTATS + SORT #################################
def listResults(vocabulaire, matrice, motInput, fonction, nbSynonymes):
    liste = []
    indexMot = vocabulaire[motInput]
    vMot = matrice[indexMot]
    methodeCalcul = selectFunction(fonction)
    for mot, index in vocabulaire.items():
            if index != indexMot:
                vCooc = matrice[index]
                if mot not in stopListe():
                    score = methodeCalcul(vMot, vCooc)
                    liste.append((mot, score))
    
    ######  ALTERNE LE TRI ENTRE CROISSANT OU DECROISSANT SELON LA FONCTION UTILISEE ######
    if fonction == "0" :
        boolSort = True;
    elif (fonction == "1") or (fonction == "2"): 
        boolSort = False;
            
    sortedList = sorted(liste,key=itemgetter(1), reverse = boolSort )[:nbSynonymes]
    print("\n********** SCORE **********\n")
    for mot, score in sortedList:
        print ("('",mot, "' , ", score, ")")  

def executeManyInsertetUpdate(connexion, dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate):
    listDesCooccurrenceInsert = []
    listDesCooccurrenceUpdate = []
    
    for lesMots, valeur in dictionnairePourRequeteInsert.items():
        listDesCooccurrenceInsert.append((lesMots[0], lesMots[1], lesMots[2], valeur))
    cur = connexion.cursor()
    query = "INSERT INTO lien VALUES (:1, :2, :3, :4)"
    cur.executemany(query, listDesCooccurrenceInsert, batcherrors=True)
    
    for lesMots, valeur in dictionnairePourRequeteUpdate.items():
        listDesCooccurrenceUpdate.append((valeur, lesMots[0], lesMots[1], lesMots[2]))
    cur = connexion.cursor()
    query = 'UPDATE lien SET valeur = :1 WHERE motID1 = :2 AND motID2 = :3 AND taille = :4'
    cur.executemany(query, listDesCooccurrenceUpdate, batcherrors=True)
    
    cur.close()
    connexion.commit()
    
    
def entrainement(chemin, encodage, tailleF):
    listDesMotsARajouterDansBD = []
    dictionnairePourRequeteInsert = {}
    dictionnairePourRequeteUpdate = {}
    connexion = connectionOracle()
    
    texte = lire(chemin, encodage)
    corpus = filtre(texte)
    vocabulaire = dictionnaireBD(connexion)
    vocabulaire = dictionnaire(corpus, listDesMotsARajouterDansBD, vocabulaire)
    pourBDEcrireLesNouveauxMots(connexion, listDesMotsARajouterDansBD)
    print("Dictionnaire Fait")
    
    dictionnairePourRequeteUpdate = creerDictionnaireCooccurrence(connexion)
    dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate = creerLes2Dictionnaires(connexion, tailleF, corpus, vocabulaire, dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate)
    
    executeManyInsertetUpdate(connexion, dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate)
    print("Lien Fait")
    
    connexion.close()  
    print("Connexion Close")
    

def recherche(tailleF):
    connexion = connectionOracle()
    vocabulaire = dictionnaireBD(connexion)
    dictionnairePourMatrice = creerDictionnaireCooccurrence(connexion)
    matrice = np.zeros((len(vocabulaire)+1, len(vocabulaire)+1))
    matrice = creerMatrice(tailleF, dictionnairePourMatrice, vocabulaire, matrice)
    choix="0"
    while not choix  == 'q':
        
        msgAccueil = "\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul, \ni.e produit scalaire : 0, least squares : 1, cityblock: 2  \n\nTapez q pour quitter : \n\n"   
        choix = input(msgAccueil)
        listeInput = list(map(str, choix.split()))
        mot = listeInput[0].lower()
        
        if mot != "q":
            nbSynonymes = int(listeInput[1])
            fonction = listeInput[2]

            ######### LISTER LES RESULTATS #########
            listResults(vocabulaire, matrice, mot , fonction, nbSynonymes)
    
    connexion.close()
            
    
def verificationArgs(): 
    checkArgs = True
    les3args = [False, False, False]
    
    for i in range (1, len(sys.argv)):
        if sys.argv[i] == "-e" :
            if len(sys.argv) == 8 :
                for i in range (1, len(sys.argv)):
                    if sys.argv[i] == "-t":
                        if sys.argv[i+1].isdigit():
                            tailleF = sys.argv[i+1]
                            les3args[0] = True                 
                    if sys.argv[i] == "-enc":
                        encodage = sys.argv[i+1]
                        les3args[1] = True
                    if sys.argv[i] == "-cc":
                        chemin = sys.argv[i+1]
                        les3args[2] = True
                    if les3args[0] and les3args[1] and les3args[2]:
                        entrainement(chemin, encodage, int(float(tailleF)))
                        exit()
        elif sys.argv[i] == "-s" :
            if(len(sys.argv) == 4):
                for i in range (1, len(sys.argv)):
                    if sys.argv[i] == "-t":
                        if sys.argv[i+1].isdigit():
                            tailleF = sys.argv[i+1]
                            checkArgs = True
                        else:
                            checkArgs = False
                    else:
                        checkArgs = False
                    if checkArgs:
                        recherche(int(float(tailleF)))
    
def main():
    verificationArgs()
  
if __name__ == '__main__':
    sys.exit(main())