import sys, numpy as np
import re
import cx_Oracle
import random
import time
from random import randint

def connectionOracle():
    ip = 'localhost'
    port = 1521
    SID = 'xe'
    dsn_tns = cx_Oracle.makedsn(ip, port, SID)
    
    connexion = cx_Oracle.connect('SYSTEM', 'AAAaaa111', dsn_tns)
       
    return connexion
    
def lire(path, enc):
    f = open(path, 'r', encoding=enc)
    s = f.read()
    if s[0] == "lufeff":
        s = s[1:]
    f.close()
    return s

def filtre(texte):
    c = re.compile('[-;:,.?!/1234567^@89_0()*" ""\n\xa0"\']+', re.IGNORECASE)
    corpus = c.split(texte.lower())
    return corpus

def filtre2(texte): #stopliste
    d = {}
    c = re.compile('[,\n\r ]+', re.IGNORECASE)
    for mot in c.split(texte):
        d[mot] = 1
    return d

def dictionnaireBD(connexion):
    vocabulaire = {}
    cur = connexion.cursor()
    cur.execute('SELECT * FROM dictionnaire')
    resultatCur = cur.fetchall()
    index = 1
    for mot in resultatCur:
        vocabulaire[mot[1]] = mot[0] 
        index += 1
    cur.close()
    return vocabulaire

def stopListe():
    return filtre2(lire("stopliste.txt", "utf-8"))

def creerDictionnaireCooccurrence(connexion):
    dictionnaireCooccurrence = {}
    cur = connexion.cursor()
    cur.execute('SELECT motID1, motID2, taille, valeur  FROM lien')
    donneeDeLaTableLien = cur.fetchall()
    for i in donneeDeLaTableLien:
        dictionnaireCooccurrence[(i[0], i[1], i[2])] = i[3]
    cur.close()
    return dictionnaireCooccurrence

def creerLes2Dictionnaires(connexion, tailleF, corpus, vocabulaire, dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate):
    debut = 0
    fin = tailleF
    milieu = fin // 2  # Milieu Fenetre
    
    while fin < len(corpus):  # DEPLACE LA FENETRE DANS LE CORPUS
        indexMot = vocabulaire[corpus[milieu]]
        i = debut  # REMET LE i AU DÉBUT DE LA FENETRE
        while i < fin :  # SE DEPLACE DANS LA FENETRE
            if i != milieu:
                indexCooc = vocabulaire[corpus[i]]
                #matrice[indexMot][indexCooc] += 1

                if (indexMot, indexCooc ,tailleF) not in dictionnairePourRequeteUpdate:
                    if (indexMot, indexCooc ,tailleF) not in dictionnairePourRequeteInsert:
                        dictionnairePourRequeteInsert[(indexMot, indexCooc ,tailleF)] = 1
                    else:
                        dictionnairePourRequeteInsert[(indexMot, indexCooc ,tailleF)] += 1
                elif (indexMot, indexCooc ,tailleF) in dictionnairePourRequeteUpdate:
                    dictionnairePourRequeteUpdate[(indexMot, indexCooc ,tailleF)] += 1
            i += 1         
        debut += 1    
        milieu += 1
        fin += 1
    return (dictionnairePourRequeteInsert, dictionnairePourRequeteUpdate)

def creerMatrice(tailleF, dictionnairePourMatrice, vocabulaire):
    matrice = np.zeros((len(vocabulaire), len(vocabulaire)))
    for cooccurrence, valeur in dictionnairePourMatrice.items():
        if cooccurrence[2] == tailleF:
            matrice[cooccurrence[0]][cooccurrence[1]] = valeur
    return matrice

############# METHODES DE CALCULS #############
def scalaire(vMot, vCooc):
    return (np.vdot(vMot, vCooc))
def cityblocks(vMot, vCooc):
    return np.sum(np.absolute(vMot - vCooc))
def leastsquares(vMot, vCooc):
    return np.sum((vMot-vCooc)**2)

#FONCTION QUI RECOIT DES CENTROIDES ET RETOURNE UNE LISTE D'APPARTENANCE
def appartenances(matricePosition, matrice, vocabulaire  , activeRandom):
    
    if activeRandom:
        pass
    else:
        appartenances = np.zeros((len(vocabulaire)))
        i = 0
        for vecteur in matrice:
            l = []
            for unCentroides in matricePosition:
                l.append(leastsquares(unCentroides, vecteur))
            appartenances[i] = l.index(min(l))
            i+=1
    return appartenances


#FONCTION QUI RECOIT UNE LISTE D'APPARTENANCE ET RETOURNE DES CENTROIDES
def nouveauxCentroides(listeAppartenances, matrice):
    matricePositionCentroides = np.zeros((len(listeAppartenances), len(matrice)))
    for i in range(len(listeAppartenances)):
        matricePositionCentroides[listeAppartenances[i]] += matrice[i]

def isInVocabulaire(listedeMotsCentroides, vocabulaire):
    motpresent = True
    for mot in listedeMotsCentroides:
            if mot not in vocabulaire:
                print("Erreur: ",mot," n'est pas dans la base de mot.")
                motpresent = False
    return motpresent

def randomCentroides(nbCentroides, vocabulaire, matrice):
    matricePosition = np.zeros((nbCentroides, len(vocabulaire)))
    for x in range(nbCentroides):
            #for y in range(len(vocabulaire)):
            #    matricePosition[x][y] = matrice[randint(0, len(vocabulaire)-1)][randint(0, len(vocabulaire)-1)]
        matricePosition[x] = matrice[randint(0, len(vocabulaire)-1)]
    return matricePosition

def affichageResultat(nbCentroides, matrice, nbMotCluster, vocabulaire, listeAppartenances, matricePosition):
    #inverser le dictionnaire
    nouvDic = {}
    for key, value in vocabulaire.items():
        nouvDic[value] = key
    
    for indexCentro in range(0, nbCentroides):
        listresult = []
        i = 0
        for quelle in listeAppartenances:
            if quelle == indexCentro:
                listresult.append((nouvDic[i], leastsquares(matrice[i], matricePosition[int(quelle)]) ))
            i+=1
        listresult = sorted(listresult, key=lambda mot: mot[1])
        print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("Groupe ", indexCentro,":")
        nbMotMax = nbMotCluster
        if nbMotCluster > len(listresult):
            nbMotMax = len(listresult)
        for e in range(0, nbMotMax):
            print(listresult[e][0]," --> ", listresult[e][1])
            
    print("\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")   

def grosseBoucle(matricePosition, matrice, vocabulaire, nbCentroides, nbMotCluster):            
    start_time = time.time()
    nbMotDifferent =1
    listeAppartenances = appartenances(matricePosition, matrice, vocabulaire, False) #False car il y a des mots en pparametre    
    
    nbIteration = 0
    while nbMotDifferent != 0:
        decomptePourMoyenne = [0]*nbCentroides
        viellelisteAppartenances = listeAppartenances
        
        
        for g in range(0, len(vocabulaire)):
            matricePosition[int(listeAppartenances[g])] += matrice[g]
            decomptePourMoyenne[int(listeAppartenances[g])] +=1
        
            
        for h in range(0,nbCentroides):
            if decomptePourMoyenne[h] != 0:
                matricePosition[h] = matricePosition[h]/decomptePourMoyenne[h]
        
        listeAppartenances = appartenances(matricePosition, matrice, vocabulaire, False) #False car il y a des mots en pparametre
        
        nbMotDifferent=0
        for j in range(0, len(listeAppartenances)):
            if listeAppartenances[j] != viellelisteAppartenances[j]:
                nbMotDifferent+=1
        for h in range(0,nbCentroides):
            print("Il y a ", decomptePourMoyenne[h]," points (mots) regroupes autour du centroide no ", h,".")
        print("============================================")
        print("Iteration ",nbIteration," terminee. ",nbMotDifferent," changements de clusters.\n")
        nbIteration+=1
        
    affichageResultat(nbCentroides, matrice, nbMotCluster, vocabulaire, listeAppartenances, matricePosition)
    print("Clustering en ",nbIteration," iterations: ",time.time() - start_time," secondes.")

def centroides(isNC, nbMotCluster, tailleF, listedeMotsCentroides = "", nbCentroides = 0):
    motpresent = True
    connexion = connectionOracle()
    vocabulaire = dictionnaireBD(connexion)
    dictionnairePourMatrice = creerDictionnaireCooccurrence(connexion)
    matrice = creerMatrice(tailleF, dictionnairePourMatrice, vocabulaire)
    
    if isNC:
        matricePosition = randomCentroides(nbCentroides, vocabulaire, matrice)
        print(nbCentroides," centroides, generes aleatoirement.\n")
    else:
        nbCentroides = len(listedeMotsCentroides)
        motpresent = isInVocabulaire(listedeMotsCentroides, vocabulaire)
        if motpresent:
            matricePosition = np.zeros((nbCentroides, len(vocabulaire)))
            for h in range(0,nbCentroides):
                    matricePosition[h] += matrice[vocabulaire[listedeMotsCentroides[h]]]
    
    if motpresent:
        grosseBoucle(matricePosition, matrice, vocabulaire, nbCentroides, nbMotCluster)
    
def verificationArgs(): 
    listedeMotsCentroides = []
    les3args = [False, False, False]
    isNC = False
    
    for i in range (1, len(sys.argv)):
        for i in range (1, len(sys.argv)):
            if sys.argv[i] == "-t":
                if sys.argv[i+1].isdigit():
                    tailleF = sys.argv[i+1]
                    les3args[0] = True                 
            if sys.argv[i] == "-nc":
                if les3args[1] == False:
                    nbCentroides = sys.argv[i+1]
                    les3args[1] = True
                    isNC = True
            elif sys.argv[i] == "-m":
                if les3args[1] == False:
                    i+=1
                    while i < len(sys.argv):
                        if sys.argv[i] == "-n" or sys.argv[i] == "-t":
                            break
                        listedeMotsCentroides.append(sys.argv[i].lower())
                        i+=1
                    les3args[1] = True
                    i-=1
            if sys.argv[i] == "-n":
                nbMotCluster = sys.argv[i+1]
                les3args[2] = True
            if les3args[0] and les3args[1] and les3args[2]:
                if isNC:
                    centroides(isNC, int(float(nbMotCluster)), int(float(tailleF)), nbCentroides = int(float(nbCentroides)))
                else :
                    centroides(isNC, int(float(nbMotCluster)), int(float(tailleF)), listedeMotsCentroides = listedeMotsCentroides)
                exit()

    
def main():
    verificationArgs()
  
if __name__ == '__main__':
    sys.exit(main())